#!/bin/bash

./bin/nmsmap --json \
             --find-base "$1" \
             --placenames example-placenames.json \
     < "$2"

