/* Thanks to github.com/bdew for this code
   https://gist.github.com/bdew/69252923b4abdffd5b825b70756a5800
   via
   https://www.reddit.com/r/NoMansSkyMods/comments/phyird/not_an_actual_editor_new_save_format_simple_specs/
*/

/**
 * Helper for reading variable length sizes
 * @param data buffer to read from
 * @param start start position
 * @param val initial value from tokne
 * @returns decoded length and new read position
 */
function readLength(data, start, val) {
    if (val < 15)
        return [val, start]; // Nothing to read
    var pos = start;
    var res = val;
    var tmp = 0;
    do {
        tmp = data.readUInt8(pos++);
        res += tmp;
    } while (tmp === 255);
    return [res, pos];
}
export default function decompressSave(data) {
    var _a, _b;
    var outputs = []; // Array of decompressed buffers
    var readPtr = 0; // Current read position
    while (readPtr < data.length) {
        // Check magic number
        if (data.readUInt32LE(readPtr) !== 0xFEEDA1E5)
            throw new Error("Missing magic number at ".concat(readPtr));
        // Read header
        var compSize = data.readInt32LE(readPtr + 4); // Compressed data size
        var buffSize = data.readInt32LE(readPtr + 8); // Output size
        readPtr += 16; // 4 bytes unknown (always 0?)
        var blockEnd = readPtr + compSize; // End of current block in fiel
        var writeBuf = Buffer.alloc(buffSize); // Output buffer
        var chunk = 0; // Size of current chunk
        var offset = 0; // Copy operation offset
        var writePtr = 0; // Output buffer position
        while (readPtr < blockEnd) {
            // Read token
            var token = data.readUInt8(readPtr++);
            // Decode data length
            _a = readLength(data, readPtr, token >> 4), chunk = _a[0], readPtr = _a[1];
            // Check overflow
            if (writePtr + chunk > buffSize)
                throw new Error("Output buffer overflow");
            // Copy data to output
            data.copy(writeBuf, writePtr, readPtr, readPtr + chunk);
            writePtr += chunk;
            readPtr += chunk;
            // If we aren't at the end - read offset for copy
            if (readPtr < blockEnd) {
                offset = data.readUInt16LE(readPtr);
                readPtr += 2;
            }
            else
                break;
            // Decode copy length
            _b = readLength(data, readPtr, token & 15), chunk = _b[0], readPtr = _b[1];
            chunk += 4;
            // Check overflow
            if (writePtr + chunk > buffSize)
                throw new Error("Output buffer overflow");
            // Do copy (this can't be done with Buffer.copy since it doesn't handle overlaps)
            for (; chunk > 0; chunk--)
                writeBuf.writeUInt8(writeBuf.readUInt8(writePtr - offset), writePtr++);
        }
        // Append to outputs
        outputs.push(writeBuf.slice(0, writePtr));
    }
    // Last byte of output seems to be 0, strip it
    return Buffer.concat(outputs).slice(0, -1);
}
