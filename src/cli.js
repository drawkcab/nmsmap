import arg from 'arg';
import { DefaultDeserializer } from 'v8';
import decompressSave from './nmsdecompress';
import getPlanetInfo from './planetinfo';
import getPlanetCoords from './nmscoords';
import fs from 'fs';
import { privateDecrypt } from 'crypto';

const {stdin} = process;

const getStdinBuffer = async () => {
	if (stdin.isTTY) {
		return Buffer.alloc(0);
	}

	const result = [];
	let length = 0;

	for await (const chunk of stdin) {
		result.push(chunk);
		length += chunk.length;
	}

	return Buffer.concat(result, length);
};

async function parseArgs(rawArgs) {
    const args = arg({
		'--raw-json': Boolean,
		'--json': Boolean,
		'--svg': Boolean,
		'--html': Boolean,
		'--planet': String,
		'--find-base': String,
		'--placenames': String,
		'--update-placenames': Boolean,
		'--mapped-by': String,
	}, {argv: rawArgs.slice(2)});

	let placenames = {};
	if (args['--placenames']) {
		let p = fs.readFileSync(args['--placenames'], 'utf8');
		placenames = JSON.parse(p);
	}

    return {
		rawJson: args['--raw-json'],
		json: args['--json'],
		svg: args['--svg'],
		html: args['--html'],
		planet: args['--planet'],
		findBase: args['--find-base'],
		mappedBy: args['--mapped-by'],
		placenames: placenames,
		updatePlacenames: args['--update-placenames'] && args['--placenames'],
        data: await getStdinBuffer()
    }
}

function svgUse(useId, pos, cLong, text) {
	let t = "";
	if (text) {
		t = '<text class="'+useId+'" x="'+((pos[1]+540-cLong)%360)+'" y="'+(90-pos[0])+'" dx="0.03" dy="0.01">'+text+'</text>';
	}
	return '<use xlink:href="'+useId+'" x="'+(pos[1]+540-cLong)%360+'" y="'+(90-pos[0])+'" />'+t;
}

function drawSVG(pi,cLong) {
	let pp = "";

	let dd = [];
	pi.visits.forEach(v => {
		if (v.length>1) {
			let c = "M";
			let d = "";

			v.forEach(p => {
				d += c+" "+(p[1]+540-cLong)%360+" "+(90-p[0])+" ";
				c = "L";
			});

			dd.push(d);
		} else {
			if (v.length>0) {
				pp += svgUse("#wp", v[0],cLong);
			}
		}
	})

	dd.forEach(d => {
		pp += `<path fill="none"
		stroke="red" stroke-width="0.01" stroke-linejoin="round" stroke-linecap="round"
		marker-start="url(#mwp)" marker-end="url(#mwp)" marker-mid="url(#mwp)"
		d="${d}" />
	`;
	})

	let bb = "";
	if (pi.bases) {
		pi.bases.forEach(b => {
			bb += svgUse("#base", b.pos, cLong, b.name);
		});
	}

	let oo = "";
	if (pi.outposts) {
		pi.outposts.forEach(o => {
			let name = o.name ? o.name : o.dname;
			let svgClass = "outpost";
			switch (o.type) {
				case "^BP_ANALYSER": svgClass = "construction"; break;
				case "^BUILDBEACON": svgClass = "save-beacon"; break;
				case "^BUILDSAVE": svgClass = "save-point"; break;
				case "^BUILDSIGNAL": svgClass = "signal-booster"; break;
				default: svgClass = "outpost";
			}
			oo += svgUse("#"+svgClass, o.pos, cLong, name);
		});
	}

	let qq = "";
	if (pi.places) {
		pi.places.forEach(q => {
			let name = q.name ? q.name : q.dname;
			qq += svgUse("#"+q.type, q.pos, cLong, name);
		});
	}

	let ii = "";
	if (pi.poi) {
		pi.poi.forEach( poi => {
			if (poi.type == "^PORTAL") {
				ii += svgUse("#portal", poi.pos,cLong);
			} else {
				ii += svgUse("#poi", poi.pos,cLong);
			}
		});
	}

	return `<svg
	xmlns:xlink="http://www.w3.org/1999/xlink"
	xmlns="http://www.w3.org/2000/svg"
	xmlns:svg="http://www.w3.org/2000/svg"
	viewBox="0 0 360 180">
	<style>
		text { font-size: 0.03 }
		.base { color: red }
	</style>
	<defs>
		<marker id="mwp" viewBox="-5 -5 10 10">
			<circle cx="0" cy="0" r="4" fill="red" />
		</marker>
		<g id="wp">
			<circle fill="red" r="0.015" />
		</g>
		<g id="base">
			<circle fill="none" stroke="red" stroke-opacity="0.2" stroke-width="3" r="5" />
			<circle fill="none" stroke="red" stroke-opacity="0.2" stroke-width="0.15" r="0.25" />
			<circle fill="red" r="0.02" />
		</g>
		<g id="portal">
			<circle fill="none" stroke="black" stroke-opacity="0.2" stroke-width="3" r="5" />
			<circle fill="none" stroke="black" stroke-opacity="0.2" stroke-width="0.15" r="0.25" />
			<circle fill="black" r="0.05" />
		</g>
		<g id="outpost">
			<circle fill="none" stroke="orange" stroke-opacity="0.2" stroke-width="0.06" r="0.1" />
		</g>
		<g id="save-beacon">
			<circle fill="none" stroke="blue" stroke-opacity="0.2" stroke-width="0.06" r="0.1" />
		</g>
		<g id="save-point">
			<circle fill="none" stroke="green" stroke-opacity="0.2" stroke-width="0.06" r="0.1" />
		</g>
		<g id="signal-booster">
			<circle fill="none" stroke="aquamarine" stroke-opacity="0.2" stroke-width="0.06" r="0.1" />
		</g>
		<g id="construction">
			<circle fill="none" stroke="magenta" stroke-opacity="0.2" stroke-width="0.06" r="0.1" />
		</g>
		<g id="island">
			<circle fill="green" r="0.02" />
		</g>
		<g id="stone">
			<path fill="black" stroke="none" d="M -0.007 0.01 H 0.007 V -0.02 H -0.007 Z" />
		</g>
		<g id="pods">
			<path fill="black" stroke="none" d="M -0.015 0.01 L -0.005 0.01 L -0.005 -0.02 L -0.015 -0.02 Z M 0.005 0.01 L 0.015 0.01 L 0.015 -0.02 L 0.005 -0.02 Z" />
		</g>
		<g id="point">
			<circle fill="black" r="0.01" />
		</g>
		<g id="monument">
			<path fill="black" stroke="none" d="M -0.02 0.02 H 0.02 V 0.015 H 0.01 V -0.02 H -0.01 V 0.015 H -0.02 Z" />
		</g>
		<g id="observatory">
			<path fill="black" stroke="none" d="M -0.02 0.02 H 0.02 V 0.015 H -0.02 Z" />
			<circle fill="black" r="0.015" />
		</g>
		<g id="depot">
			<path fill="black" stroke="none" d="M -0.02 0.02 H 0.02 V 0.015 H -0.02 Z" />
			<circle fill="black" cx="-0.01" cy="0.005" r="0.007" />
			<circle fill="black" cx="0.01" cy="0.005" r="0.007" />
			<circle fill="black" cx="0" cy="-0.005" r="0.007" />
		</g>
		<g id="ruin">
			<path fill="black" stroke="none" d="M -0.02 0.02 H 0.02 V 0.015 H 0.01 V -0.01 L -0.01 0.005 V 0.015 H -0.02 Z" />
		</g>
		<g id="monolith">
			<path fill="black" stroke="none" d="M -0.02 0.02 L 0.02 0.02 L 0.02 0.015 L -0.02 0.015 Z M 0 0.015 L 0.02 -0.01 L 0 -0.04 L -0.02 -0.01 Z" />
		</g>
		<g id="freighter">
			<path fill="blue" stroke="none" d="M -0.04 0.02 H 0.02 V 0.015 L 0.01 -0.02 L 0 -0.015 L 0.01 0 L -0.01 0.015 L -0.04 -0.01 Z" />
		</g>
		<g id="starbulb">
			<circle fill="cyan" r="0.04" />
			<text text-anchor="middle" dy="0.01">SB</text>
		</g>
		<g id="hill">
			<path fill="black" stroke="none" d="M -0.02 0.02 L 0.02 0.02 L 0 -0.02 Z" />
		</g>
		<g id="poi">
			<circle fill="none" stroke="magenta" stroke-opacity="0.2" stroke-width="3" r="5" />
			<circle fill="magenta" r="0.05" />
		</g>
	</defs>
	${pp}
	${oo}
	${qq}
	${bb}
	${ii}
</svg>`;

}

function drawHTML(pi, cLong) {
	return `<!DOCTYPE html>
<html>
	<head>
		<title>NMSdec</title>
	</head>
	<body>
		${drawSVG(pi, cLong)}
	</body>
</html>`
}

function describePlanet(p, args, cLong) {
	if (args.json) {
		return JSON.stringify(p, null, 2);
	}

	if (args.svg) {
		return drawSVG(p, cLong);
	}

	if (args.html) {
		return drawHTML(p, cLong);
	}
}

export async function cli(rawArgs) {
    let args = await parseArgs(rawArgs);

	let rawJson = decompressSave(args.data).toString('utf-8');
	if (args.rawJson) {
		console.log(rawJson);
		return;
	}

    const data = JSON.parse(rawJson);

	let addToPn = {};
	if (args.mappedBy) { addToPn.mappedBy = args.mappedBy };

	const pi = getPlanetInfo(data, args.placenames, addToPn);

	Object.keys(pi).forEach( planetId => {
		pi[planetId].location = getPlanetCoords(planetId);
	});

	let planet;
	let cLong = 0;

	if (args.planet) {
		if (pi[args.planet]) {
			planet = pi[args.planet];
		}
	}

	if (args.findBase) {
		Object.keys(pi).forEach(p => {
			if (pi[p].bases) {
				pi[p].bases.forEach(b => {
					if (b.name.includes(args.findBase)) {
						if (planet) {
							console.log("Warning: found multiple matches, discarding",b.name);
						} else {
							planet = pi[p];
							cLong = b.pos[1];
						}
					}
				})
			}
		})
	}

	if (planet) {
		console.log(describePlanet(planet, args, cLong));

		if (args.updatePlacenames) {
			fs.writeFileSync(args.updatePlacenames, JSON.stringify(args.placenames, null, 4), 'utf8');
		}	
	}

}