import { Coordinate } from 'coordinate-systems';

function normPlanetId(planetId) {
    let p = planetId;
    if (typeof planetId === "string") {
        p = parseInt(planetId,16); // either 0xFFF or FFF will work
    }
    p = ('00000000000000' + p.toString(16).toUpperCase()).slice(-14);
    return p;
}

function latLong(wMC) {
    let xyz = Coordinate.cartesian([wMC[0],wMC[2],wMC[1]]);
    let rtp = xyz.spherical();

    let lat = 90 - (rtp[2] * 180) / Math.PI;
    let lon = 90 - (rtp[1] * 180) / Math.PI;

    return [ lat, lon, rtp[0] ];
}

function dateName(d) {
    const c = [ "b" ,"d" ,"f" ,"g" ,"j" ,"k" ,"l" ,
                "m" ,"n" ,"p" ,"s" ,"t" ,"v" ,"z" ,"bl",
                "br","ch","cl","cr","dr","fr","gl","gr",
                "pl","pr","sk","sh","sp","st","th","tr", "fl", "b"];
    const v = ["a", "e", "i", "o", "u", "ow", "ai"];

    return c[d.getDate()]+v[d.getDay()]+c[d.getMonth()]+" "+c[d.getHours()]+v[Math.floor(d.getMinutes()/12)]+c[d.getMinutes()%12]+v[Math.floor(d.getSeconds()/12)]+c[d.getSeconds()%12];
}

export default function getPlanetInfo(data, pn, addToPn) {
    let res = {};
    data["6f="]["gUR"].forEach(d => {
        if (d[":rc"]=="^PLANET_STATS") {
            let planetId = normPlanetId(d["2Ak"]);
            if (!res[planetId]) { res[planetId] = { id: planetId } };
            if (!res[planetId].stats) { res[planetId].stats = {} };

            d["gUR"].forEach(s => {
                let v = s[">MX"];
                if (v[">vs"]) { res[planetId].stats[s["b2n"]] = v[">vs"]; } // integer
                if (v["eoL"]) { res[planetId].stats[s["b2n"]] = v["eoL"]; } // float
            })
        }
    });

    let discoveries = data["fDu"]["ETO"]["OsQ"]["?fB"];

    discoveries.forEach(d => {
        if (d["8P3"]["<Dn"] == "Planet") {
            let planetId = normPlanetId(d["8P3"]["5L6"]);
            if (!res[planetId]) { res[planetId] = { id: planetId } };
            if (d["q9a"] && d["q9a"]["q5u"]) {
                res[planetId]["custom_name"] = d["q9a"]["q5u"];
            }
            if (d["ksu"] && d["ksu"]["V?:"]) {
                res[planetId]["discoverer"] = d["ksu"]["V?:"];
            }
        }
    })

    let bases = data["6f="]["F?0"];

    bases.forEach(b => {
        let planetId = normPlanetId(b["oZw"]);
        if (!res[planetId]) { res[planetId] = { id: planetId } };
        if (!res[planetId].bases) { res[planetId].bases = [] };

        let baseName = b["NKm"];
        res[planetId].bases.push({
            name: baseName,
            pos: latLong(b["wMC"]),
            wMC: b["wMC"]
        });
    });

    let outposts = data["6f="]["c5s"];

    let now = new Date();

    outposts.forEach(o => {
        let planetId = normPlanetId(o["oZw"]);
        if (!res[planetId]) { res[planetId] = { id: planetId } };
        if (!res[planetId].outposts) { res[planetId].outposts = [] };

        let outpostType = o["r<7"];
        let ts = new Date(o["b1:"] *1000);
        let dname = dateName(ts);
        if (!pn[dname]) {
            pn[dname] = {
                recorded: ts,
                mapped: now,
                ...addToPn
            };
        }
        pn[dname].pos = latLong(o["wMC"]);
        pn[dname].wMC = o["wMC"];
        pn[dname].planet = planetId;
        pn[dname].checked = now;

        res[planetId].outposts.push({
            type: outpostType,
            pos: latLong(o["wMC"]),
            wMC: o["wMC"],
            planet: planetId,
            id: o["b1:"],
            ts: ts,
            dname: pn[dname].type ? "" : dname
        });
    })

    Object.keys(pn).forEach(dname => {
        let p = pn[dname];
        if (!p.mapped) { p.mapped = now };
        if (p.planet) {
            let planetId = normPlanetId(p.planet);
            if (!res[planetId]) { res[planetId] = { id: planetId } };
            if (!res[planetId].places) { res[planetId].places = [] };
            res[planetId].places.push(p);
        }
    })

    /* "TWn" = buildingLocation has a different format from "wMC" = position
        which i have not worked out

    let poi = data["6f="]["A1f"];

    poi.forEach(o => {
        let planetId = normPlanetId(o["oZw"]);
        if (!res[planetId]) { res[planetId] = { id: planetId } };
        if (!res[planetId].poi) { res[planetId].poi = [] };

        let poiType = o["FN5"];
        res[planetId].poi.push({
            type: poiType,
            pos: latLong(o["TWn"]),
            TWn: o["TWn"]
        });
    })

    */ 
   
    let visitChunks = data["6f="]["a6j"];

    let lastSeen = {};

    visitChunks.forEach(vc => {
        vc["O49"].forEach(v => {
            if (v["oZw"]) {
                let planetId = normPlanetId(v["oZw"]);
                if (!res[planetId]) { res[planetId] = { id: planetId } };
                if (!res[planetId].visits) { res[planetId].visits = [] };

                let xyz = v["wMC"];

                if (!lastSeen[planetId]) {
                    res[planetId].visits.push([latLong(xyz)]);
                } else {
                    let ll = latLong(xyz);
                    let oll = latLong(lastSeen[planetId]);
                    let dl = Math.sqrt((ll[0]-oll[0])**2+(ll[1]-oll[1])**2);
                    let dr = Math.abs(ll[2]-oll[2]);

                    if (dl>0.3) {
                        res[planetId].visits.push([latLong(xyz)]);
                    } else {
                        res[planetId].visits[res[planetId].visits.length-1].push(latLong(xyz));
                    }
                }
                lastSeen[planetId] = xyz;
            }
        })
    })
    return res;
}