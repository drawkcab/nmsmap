function leftPad(h) {
    return ('0000' + h.toString(16).toUpperCase()).slice(-4);
}

export function getPortalCoords(planetId) {
    return planetId.slice(0,4)+planetId.slice(6);
}

export default function getPlanetCoords(planetId) {
    let y = (parseInt(planetId.slice(6,8),16)+0x7f)%0x100;
    let z = (parseInt(planetId.slice(8,11),16)+0x7ff)%0x1000;
    let x = (parseInt(planetId.slice(11,14),16)+0x7ff)%0x1000;
    let si = parseInt(planetId.slice(1,4),16);

    return {
        galaxy: parseInt(planetId.slice(4,6),16)+1,
        x: x,
        y: y,
        z: z,
        systemIndex: parseInt(planetId.slice(1,4),16),
        planetIndex: parseInt(planetId[0],16),
        portal: getPortalCoords(planetId),
        galactic: leftPad(x)+":"+leftPad(y)+":"+leftPad(z)+":"+leftPad(si)
    }
}