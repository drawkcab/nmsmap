#!/bin/bash

export SVGFILE="$(basename $1 .hg).svg"

./bin/nmsmap --svg \
             --find-base Anmaria \
             --placenames example-placenames.json \
     < "$1" > "$SVGFILE"
inkscape "$SVGFILE" &
