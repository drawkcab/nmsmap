## Nmsmap v0.0

Draws maps of places you have visited on your No Man's Sky save.

See the example scripts for basic usage.

### Install

Written with node 18/ESM. should just be able to run

    npm install

and then the scripts should work.

### Run

Pipe your save file into `bin/nmsmap`

    ./bin/nmsmap [OPTIONS] < [SAVEFILE]

Main output goes to stdout.

On Linux/Steam, the save file is probably somewhere like

    ~/.local/share/Steam/steamapps/compatdata/275850/pfx/drive_c/users/steamuser/AppData/Roaming/HelloGames/NMS/st_000000000000000000000/save.hg

### Options

* `--json` : Output JSON. Mostly useful for debugging.
* `--svg` : Output SVG.
* `--planet` : Planet to map (convert portal glyphs to hex 0-9A-F)
* `--find-base` : Planet to map, given the name of a base on that planet (must be in the save file so best use one of your own)
* `--placenames [FILE]` : JSON file containing named places and their coordinates. See `example-placenames.json`
* `--update-placenames` : Rewrite the above file to add any unknown places on the mapped planet that are in the save file. Just add your own names.

### What places will be mapped?

* Bases (i.e. base computers)
* Save points
* Save beacons
* Technology research doodads
* Probably some other portable technologies I forget. These all get circles of different colours.
* Locations where you have interacted with planetary entities (destroyed a plant, pinged a knowledge stone, etc.) These get smaller dots linked together in a path. Poor Exocraft driving skills will be rewarded.
* Any locations listed in the placenames file

The last of these will eventually get scrubbed from your save.

Terrain manipulations are not yet mapped.

Portals are mapped but the locations are completely wrong :doh:

### How do I map a planet?

Something like

1. Drop some portable technologies in-game. End with a save point or beacon. Save your position.
2. Switch to terminal and run `nmsmap` with `--svg` and `--update-placenames`
3. Edit the updated placenames file to give names to the listed places. Use the SVG map to help you remember which place is which. Don't change the keys.
4. Switch back to the game and pick up your stuff. Leave no trace :)
5. Repeat

### skaik what now?

The keys in the placenames file are derived from the first mapped date as this is the best available ID. As long as you don't destroy more than one thing in a second these will be unique.

### What is left to do?

* The SVG always includes the whole planet. Chances are only a tiny bit of planet is going to be mapped, so a smart window feature would be useful.
* HTML output. Or React or something fancy.
* Hello Games should let you just name places on planets. That would make all this a lot easier.
* Also they should let every player have their own name for things. This whole "first interloper gets to rename stuff forever" is sketch af.
